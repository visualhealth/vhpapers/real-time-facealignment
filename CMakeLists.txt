# Repository associated to the manuscript (currently under review):
# “Real-time face alignment: evaluation methods, training strategies and implementation optimization"

# Authors:
# Constantino Álvarez Casado, Miguel Bordallo López
# The repository contains original code, benchmarks and links to the original and improved models .

#
# Copyright (C)  University of Oulu 2021
#
#

cmake_minimum_required(VERSION 3.10.0)

project(real-time-facealignment)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)


message(STATUS "System name: " ${CMAKE_SYSTEM_NAME} )
message(STATUS "System processor: " ${CMAKE_SYSTEM_PROCESSOR} )
message(STATUS "Project name: " ${PROJECT_NAME} )


if (WIN32)
    set_property(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY VS_STARTUP_PROJECT ${PROJECT_NAME})
    add_definitions(
        -DNOMINMAX
        -DFARMHASH_NO_BUILTIN_EXPECT
        )
endif()


set( FLAGS_COMMON

    ## General Options
    #-DVERBOSE

    ## Benchmark options
    -DVISUALIZATION

    )


set(CMAKE_VERBOSE_MAKEFILE on)
option(USE_OPENCV "Enable OpenCV for visualizing results" OFF)
option(USE_DLIB "Enable Dlib for cv algorithms" OFF)
option(USE_OPENMP "Enable OpenMP parallel computing" OFF)
option(USE_FAST_MATH "Enable fast-math gcc flag" OFF)

set(USE_OPENCV ON)
set(USE_DLIB ON)
set(USE_OPENMP ON)
set(USE_FAST_MATH OFF)


if(USE_OPENMP)
    find_package(OpenMP REQUIRED)
endif()



if (${CMAKE_SYSTEM_NAME} STREQUAL "Windows")

    add_definitions(-DVD_WINDOWS)
    add_compile_options(/fp:fast)      #Floating point model fast
    #add_compile_options(/arch:SSE2)    #Enable SSE2 (no SSE3 / SSE4.1 on VS?)
    add_compile_options(/arch:AVX2)   #Enable AVX

    add_compile_options(/wd4309 /wd4324 /wd4389 /wd4127 /wd4267 /wd4146 /wd4201 /wd4464 /wd4514 /wd4701 /wd4820 /wd4365 /wd4244)
    add_definitions(-D_CRT_SECURE_NO_WARNINGS)
    add_compile_options(/MP)   #parallel build
else()

     if(USE_FAST_MATH)
         add_compile_options("$<$<COMPILE_LANGUAGE:CXX>:-ffast-math>")
         add_compile_options("$<$<COMPILE_LANGUAGE:C>:-ffast-math>")
     endif()
    #add_compile_options("$<$<COMPILE_LANGUAGE:CXX>:-funroll-loops;-Wno-write-strings;-ffast-math;-fno-omit-frame-pointer>")
    #add_compile_options("$<$<COMPILE_LANGUAGE:C>:-funroll-loops;-Wno-write-strings;-ffast-math;-fno-omit-frame-pointer>")

    message(${CMAKE_SYSTEM_PROCESSOR})
    if(${CMAKE_SYSTEM_PROCESSOR} STREQUAL "x86_64")

        add_compile_options("$<$<COMPILE_LANGUAGE:CXX>:-march=native>")
        add_compile_options("$<$<COMPILE_LANGUAGE:C>:-march=native>")

    endif()

endif()




if(USE_OPENCV)
    #find_package(OpenCV 2.4 REQUIRED)
    ############
    ## OPENCV ##
    ############
    set(OPENCV_VERSION opencv_4_5_2)
    set(OpenCV_DIR ${CMAKE_CURRENT_LIST_DIR}/../../../../../Libraries/${OPENCV_VERSION}/lib/cmake/opencv4/)
    find_package(OpenCV REQUIRED)
    include_directories( ${OpenCV_INCLUDE_DIRS} )


    # If the package has been found, several variables will
    # be set, you can find the full list with descriptions
    # in the OpenCVConfig.cmake file.
    # Print some message showing some of them
    message(STATUS "OpenCV library status:")
    message(STATUS "    version: ${OpenCV_VERSION}")
    message(STATUS "    libraries: ${OpenCV_LIBS}")
    message(STATUS "    include path: ${OpenCV_INCLUDE_DIRS}")

endif()


if(USE_DLIB)
    ############
    ##  DLIB  ##
    ############
    set(DLIB_VERSION dlib_19_22)
    set(dlib_DIR ${CMAKE_CURRENT_LIST_DIR}/../../../../../Libraries/${DLIB_VERSION}/lib/cmake/dlib)

    find_package(dlib REQUIRED)

    # If the package has been found, several variables will
    # be set, you can find the full list with descriptions
    # in the OpenCVConfig.cmake file.
    # Print some message showing some of them
    message(STATUS "Dlib library status:")
    message(STATUS "    version: ${DLIB_VERSION}")
endif()


set(BUILD_TYPE SHARED)
add_definitions( ${FLAGS_COMMON} )


set( SOURCES
    ${CMAKE_CURRENT_LIST_DIR}/main.cpp
    ${CMAKE_CURRENT_LIST_DIR}/src/common.cpp
    ${CMAKE_CURRENT_LIST_DIR}/src/testing.cpp
    ${CMAKE_CURRENT_LIST_DIR}/include/common.hpp
    ${CMAKE_CURRENT_LIST_DIR}/include/testing.hpp
    )


cmake_policy(SET CMP0071 NEW)

add_executable(${PROJECT_NAME}
               ${SOURCES}
               )


# add lib dependencies
if (${CMAKE_SYSTEM_NAME} STREQUAL "Windows")
    target_link_libraries(${PROJECT_NAME}  jpeg)
else()
    target_link_libraries(${PROJECT_NAME}
        ${OpenCV_LIBS}
        dlib::dlib
        jpeg

        )
    #-fsanitize=address
endif()
