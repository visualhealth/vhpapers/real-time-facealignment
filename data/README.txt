Folder to place the data.

Notes:
    - Download public databases from: https://ibug.doc.ic.ac.uk/resources/facial-point-annotations/
    - Download private datasets from: https://drive.google.com/drive/folders/1lhgt8aZRuyKRMA43JRvF79Uh9zldk7fa?usp=sharing
    - We already provide the annotations ready to read from the project in .txt and .xml formats.

